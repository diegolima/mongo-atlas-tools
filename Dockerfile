FROM mongo:latest

RUN apt-get update \
  && apt-get install -y \
    python3 \
    python3-pip \
  && rm -rf /var/lib/apt/lists/*
RUN pip install requests
COPY assets/add_ip_to_mongo.py /usr/local/bin