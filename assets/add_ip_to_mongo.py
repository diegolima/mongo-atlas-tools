#!/usr/bin/env python

import json
import os
import subprocess
from datetime import datetime
from typing import List

import requests
from requests.auth import HTTPDigestAuth


def get_output(cmd, ignore_erro=False):
    try:
        proc = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
        return proc[:-1].decode()
    except Exception as e:
        if not ignore_erro:
            print(e.output)
            raise e


def check_current_public_ip() -> List[str]:

    ips = []
    cmd = (
        "dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | awk -F"
        + "'"
        + '"'
        + "' '{ print $2}'"
    )

    for x in range(11):
        r = get_output(cmd)
        ips.append(r)

    ips = list(set(ips))
    print(ips)
    return ips


PUBLIC_KEY = os.getenv("ATLAS_PUBLIC_KEY")
PRIVATE_KEY = os.getenv("ATLAS_PRIVATE_KEY")
PROJECT_ID_ATLAS = os.getenv("ATLAS_PROJECT_ID")

auth = (PUBLIC_KEY, PRIVATE_KEY)

url = f"""https://cloud.mongodb.com/api/atlas/v1.0/groups/{PROJECT_ID_ATLAS}/accessList?pretty=true"""

headers = {"Accept": "application/json", "Content-Type": "application/json"}
now = datetime.now()


if not PUBLIC_KEY:
    print("Não foram setadas as variaveis PUBLIC_KEY ")
    exit(1)

for ip in check_current_public_ip():
    print(ip)

    myhost = os.uname()[1]
    comment = f"{os.getlogin()}-{myhost}"

    body = [
        {
            "ipAddress": ip,
            "comment": comment,
            "deleteAfterDate": datetime(
                now.year, now.month, now.day, 23, 59, 59
            ).isoformat(),
        }
    ]

    result = requests.post(
        url,
        headers=headers,
        auth=HTTPDigestAuth(PUBLIC_KEY, PRIVATE_KEY),
        data=json.dumps(body),
    )
    print(result.status_code)
